##  Lancer l'application
Pour lancer l'application, il faut  éxecuter la commande suivante:  
### `yarn && yarn start`

##  Story book interne
Afin de debugger les différents composants qui constituent notre application, on peut lancer le storybook en executant la commande:

### `yarn run storybook`

