import { call } from "redux-saga/effects";

export interface IHeader {
  method: string;
  body: any
}

export function* fetchData(url: string) {
    const data = yield call(fetch, url);
    if (data.status >= 200 && data.status < 300) {
      return yield data.json();
    };
    throw data;
}