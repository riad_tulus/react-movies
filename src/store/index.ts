import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { connectRouter, routerMiddleware, RouterState } from 'connected-react-router';
import { all } from 'redux-saga/effects';
import { IMoviesState } from '../containers/movies/types';
import moviesReducer from '../containers/movies/reducer';
import movieDetailsReducer from '../containers/movie-details/reducer';
import { watchMoviesSagas } from '../containers/movies/saga';
import { IMovieDetailsState } from '../containers/movie-details/types';
import { watchMovieDetailsSagas } from '../containers/movie-details/saga';

export interface RootReducer {
  moviesReducer: IMoviesState; 
  movieDetailsReducer: IMovieDetailsState; 
  router: RouterState;
}

function* rootSaga() {
  yield all([watchMoviesSagas(), watchMovieDetailsSagas()]);
}

const rootReducer = (history: any) =>
  combineReducers({
    moviesReducer,
    movieDetailsReducer,
    router: connectRouter(history),
  });

const sagaMiddleware = createSagaMiddleware();

export const store = (history: any) => {
 const store = createStore(
    rootReducer(history),
    composeWithDevTools(applyMiddleware(sagaMiddleware, routerMiddleware(history))),
  );
  sagaMiddleware.run(rootSaga);
  return store;
}
