import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { MoviesContainer } from './containers/movies/'
import { store } from './store/index';
import { AppHeader } from './component-ui/header';
import { MovieDetailsContainer } from './containers/movie-details';

export const history = createBrowserHistory();

function App() {
  return (
    <Provider store={store(history)}>
      <AppHeader />
      <ConnectedRouter history={history}>
          <Route exact path="/" component={MoviesContainer} />
          <Route exact path="/:id" component={MovieDetailsContainer} />
      </ConnectedRouter>
      </Provider>
  );
}

export default App;
