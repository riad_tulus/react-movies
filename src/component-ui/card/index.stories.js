import React from "react";
import { Card } from ".";
import { Movie } from "../../models/movie";

export default {
  title: "Card",
  component: Card,
};

const img_path = "/xBHvZcjRiWyobQ9kxBhO6B2dtRI.jpg";
const rootStyle = { width: "14rem" };

const movieMock = new Movie({
  id: 1,
  poster_path: img_path,
  title: "casa de papel",
  popularity: 100.0,
  vote_count: 100,
  release_date: "12/12/2019",
});

export const card = () => (
  <div style={rootStyle}>
    <Card movie={movieMock} />
  </div>
);
