import React from "react";
import {
  Container,
  Picture,
  Title,
  Vote,
  ReleaseDate,
  Details,
  Popularity,
} from "./styles";
import { ReactComponent as Like } from "../../icons/like.svg";
import { ReactComponent as Seen } from "../../icons/eye.svg";
import { Movie } from "../../models/movie";

interface ICard {
  key: number;
  movie: Movie;
  onClick: (movie: Movie) => void;
}

const Card = ({ movie, onClick }: ICard) => {
  // HANDLERS
  const handleClick = () => onClick(movie);

  // RENDERS
  return (
    <Container key={movie.id} posterPath={movie.poster} onClick={handleClick}>
      <Picture />
      <Details className="card-details">
        <Title>{movie.title}</Title>
        <ReleaseDate>{movie.date}</ReleaseDate>
        <Vote>
          <Like style={{ fill: "white" }} />
          <p>{movie.voteCount}</p>
        </Vote>
        <Popularity>
          <Seen style={{ fill: "white" }} />
          <p>{movie.popularity}</p>
        </Popularity>
      </Details>
    </Container>
  );
};

export { Card };
