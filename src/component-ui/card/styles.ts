import styled from 'styled-components';

const Container = styled.div<{posterPath: string}>`
    height: 18em;
    width: 14em;
    font-family: PT Sans;
    box-shadow: 0px 1px 5px 0px rgba(0,0,0,0.75);
    background-image: url(${(props: any) => props.posterPath});
    background-size:100% 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    color: white;
    border-radius: 12px;
    &:hover .card-details {
      height:100%;
      transition: all 0.4s ease-out;
      cursor: pointer;
    }
`

const Details = styled.div`
    display: grid;
    height:0%;
    overflow: hidden;
    grid-template-areas:    "title title"
                            "date date"
                            "vote popularity";
    grid-template-rows: auto auto 5rem;
    grid-template-columns: 1fr 1fr;
    grid-gap: 1em;
    background-color: rgba(105,105,105, 0.7);
    border-radius: 12px;
`;

const Picture = styled.div`
    min-width: 100%;
    grid-area: picture;
`

const Title = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    grid-area: title;
    font-size: 17pt;
    text-align: center;
    font-weight: bold;
`

const ReleaseDate = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    grid-area: date;
`

const Vote = styled.div`
    grid-area: vote;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    flex-direction: column;
`

const Popularity = styled.div`
    grid-area: popularity;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    flex-direction: column;
`

export { Container, Picture, Title, ReleaseDate, Vote, Details, Popularity }