import React from "react";
import { AppHeader } from ".";

export default {
  title: "Header",
  component: AppHeader
};


export const HeaderStand = () => (
    <div>
      <AppHeader />
    </div>
);
