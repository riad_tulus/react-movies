import React from "react";
import { Container, HomeWrapper } from "./styles";
import { ReactComponent as Home } from "../../icons/browser.svg";
import { history } from '../../App';

const AppHeader = () => {
  const handleClick = () => history.push('/');
  return (
    <Container>
      <HomeWrapper onClick={handleClick}>
        <Home style={{ fill: "white" }} />
        TEST CANAL+
      </HomeWrapper>
    </Container>
  );
};

export { AppHeader };
