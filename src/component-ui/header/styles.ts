import styled from 'styled-components';

const Container = styled.div`
    width: 100%;
    height: 6rem;
    background-color: black;
    box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.75);
    color: white;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-size: 20pt;
`

const HomeWrapper = styled.div`
    margin-left: 3rem;
    width: 20%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    &:hover {
        cursor: pointer;
    }
`;
export { Container, HomeWrapper }