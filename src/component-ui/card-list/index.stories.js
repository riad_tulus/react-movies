import React from "react";
import { CardList } from ".";
import { Movie } from "../../models/movie";

export default {
  title: "CardList",
  component: CardList,
};

const img_path =
  "/xBHvZcjRiWyobQ9kxBhO6B2dtRI.jpg";
const rootStyle = { width: '100%' };

const movies = [
  new Movie({
    id: 2,
    poster_path: img_path,
    title: "casa de papel",
    popularity: 100.0,
    vote_count: 100,
    release_date: "12/12/2019",
  }),
  new Movie({
    id: 3,
    poster_path: img_path,
    title: "casa de papel",
    popularity: 100.0,
    vote_count: 100,
    release_date: "12/12/2019",
  }),
  new Movie({
    id: 4,
    poster_path: img_path,
    title: "casa de papel",
    popularity: 100.0,
    vote_count: 100,
    release_date: "12/12/2019",
  }),
];

export const cardList = () => (
  <div style={rootStyle}>
    <CardList movies={movies} />
  </div>
);
