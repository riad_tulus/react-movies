import styled from 'styled-components';


const Container = styled.div`
    width: 100%;
    display: grid;
    align-items: center;
    grid-template-columns: repeat(auto-fit, minmax(240px, 1fr));
    grid-gap: 2rem;
    justify-items: center;
`;

export { Container }