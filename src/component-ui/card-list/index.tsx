import React from 'react';
import { Movie } from '../../models/movie';
import { Card } from '../card';
import { Container } from './styles';
import { Loader } from '../loader';

interface ICardList {
    movies: Array<Movie>
    isLoading: boolean; 
    fetchDetails: (movie: Movie) => void;
}

const CardList = ({ movies, isLoading, fetchDetails }: ICardList) => {
    return <Container>
        {
            isLoading ? <Loader /> : movies.map((movie: Movie) => <Card key={movie.id} movie={movie} onClick={fetchDetails}/>)
        }
    </Container>
}

export { CardList }