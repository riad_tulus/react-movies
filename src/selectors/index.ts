import { RootReducer } from "../store";

export const selectMovies = ({ moviesReducer }: RootReducer) => moviesReducer.movies;

export const selectMovieDetails = ({ movieDetailsReducer }: RootReducer) => movieDetailsReducer.movie;

export const selectLoadingStatus = ({ moviesReducer }: RootReducer) => moviesReducer.isLoading;

export const selectMovieDetailsLoadingStatus = ({ movieDetailsReducer }: RootReducer) => movieDetailsReducer.isLoading;
