import { Movie } from "../../models/movie";
import { Action } from "redux";

export interface IMovieDetailsState {
    movie: Movie;
    isLoading: boolean;
    errorMessage: string;
  }

export interface IFetchMovieDetails extends Action {
    readonly type: MovieDetailsActionEnum.FETCH_MOVIE_DETAILS;
    readonly payload: number;
  }
  
  export interface IFetchMovieDetailsSuccess extends Action {
    readonly type: MovieDetailsActionEnum.FETCH_MOVIE_DETAILS_SUCCESS;
    readonly payload: Movie;
  }
  
  export interface IFetchMovieDetailsFail extends Action {
    readonly type: MovieDetailsActionEnum.FETCH_MOVIE_DETAILS_FAIL;
    readonly payload: Error;
  }

  export enum MovieDetailsActionEnum {
    FETCH_MOVIE_DETAILS = 'FETCH_MOVIE_DETAILS',
    FETCH_MOVIE_DETAILS_SUCCESS = 'FETCH_MOVIE_DETAILS_SUCCESS',
    FETCH_MOVIE_DETAILS_FAIL = 'FETCH_MOVIE_DETAILS_FAIL'
  }

  export type MovieDetailsAction =
  | IFetchMovieDetails
  | IFetchMovieDetailsSuccess
  | IFetchMovieDetailsFail
  ;