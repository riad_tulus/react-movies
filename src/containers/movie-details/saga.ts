import { takeEvery, put } from "redux-saga/effects";
import { MovieDetailsActionEnum, IFetchMovieDetails } from "./types";
import { fetchData } from "../../utils/httpcalls";
import { apiUrl, apiKey } from "../../config";
import { fetchMovieDetailsSuccess, fetchMovieDetailsFail } from "./action";
import { Movie } from "../../models/movie";

function* fetchMovieDetails(action: IFetchMovieDetails) {
    try {
      const movie = yield fetchData(`${apiUrl}movie/${action.payload}?api_key=${apiKey}`);
     
      yield put(fetchMovieDetailsSuccess(new Movie(movie)));
    } catch (error) {
      yield put(fetchMovieDetailsFail(error));
    }
  }

  export function* watchMovieDetailsSagas() {
    yield takeEvery(MovieDetailsActionEnum.FETCH_MOVIE_DETAILS, fetchMovieDetails);
  }