import { MovieDetailsAction, MovieDetailsActionEnum, IMovieDetailsState } from "./types";
import { Movie } from "../../models/movie";

export const defaultMovieDetailsState: IMovieDetailsState = {
  movie: {} as Movie,
  isLoading: false,
  errorMessage: "",
};

const moviesReducer = (
  state: IMovieDetailsState = defaultMovieDetailsState,
  action: MovieDetailsAction
): IMovieDetailsState => {
  switch (action.type) {
    case MovieDetailsActionEnum.FETCH_MOVIE_DETAILS:
      return {
        ...state,
        isLoading: true,
      };
    case MovieDetailsActionEnum.FETCH_MOVIE_DETAILS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        movie: action.payload,
      };
    case MovieDetailsActionEnum.FETCH_MOVIE_DETAILS_FAIL:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload.message,
      };
    default:
      return state;
  }
};

export default moviesReducer;
