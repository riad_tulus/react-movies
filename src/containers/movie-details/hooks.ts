import { useDispatch, useSelector } from "react-redux";
import { fetchMovieDetails } from "./action";
import { useEffect } from "react";
import { selectMovieDetails, selectMovieDetailsLoadingStatus } from "../../selectors";

export const useMovieDetailsFetch = (movieId: number) => {
    const dispatch = useDispatch();
    function ComponentDidMount() {
      dispatch(fetchMovieDetails(movieId));
    }
    useEffect(ComponentDidMount, []);

    const movie = useSelector(selectMovieDetails);
    const isLoading = useSelector(selectMovieDetailsLoadingStatus);

    return {movie, isLoading};
  };