import { Movie } from "../../models/movie";
import { IFetchMovieDetails, MovieDetailsActionEnum, IFetchMovieDetailsSuccess, IFetchMovieDetailsFail } from "./types";

export const fetchMovieDetails = (movieId: number): IFetchMovieDetails => ({
    type: MovieDetailsActionEnum.FETCH_MOVIE_DETAILS,
    payload: movieId
  });
  
  export const fetchMovieDetailsSuccess = (payload: Movie): IFetchMovieDetailsSuccess => ({
    type: MovieDetailsActionEnum.FETCH_MOVIE_DETAILS_SUCCESS,
    payload,
  });
  
  export const fetchMovieDetailsFail = (errorMessage: Error): IFetchMovieDetailsFail => ({
    type: MovieDetailsActionEnum.FETCH_MOVIE_DETAILS_FAIL,
    payload: errorMessage,
  });
  