import styled from 'styled-components';

const Container = styled.div`
    display: grid;
    padding: 5rem;
    grid-template-areas:    "poster title"
                            "poster vote"
                            "poster date"
                            "poster popularity"
                            "description description";
    grid-template-columns: 18rem 1fr;
    grid-template-rows: 5rem 5rem 5rem 5rem 10rem;
    grid-gap: 1em;
    font-size: 17pt;
    justify-content: center;
`

const Poster = styled.div<{posterPath: string}>`
    grid-area: poster;
    grid-row: 1 / 5;
    background-image: url(${(props: any) => props.posterPath});
    background-size:100% 100%;
`;

const Title = styled.div`
    grid-area: title;
    font-size: 30pt;
    display: flex;
    align-items: center;
`;

const Vote = styled.div`
    grid-area: vote;
    display: flex;
    align-items: center;
`;

const Popularity = styled.div`
    grid-area: popularity;
    display: flex;
    align-items: center;
`;

const ReleaseDate = styled.div`
    grid-area: date;
    display: flex;
    align-items: center;
`;

const Description = styled.div`
    grid-area: description;
    grid-row: 5 / 5;
`;

export { Container, Poster, Title, Popularity, Vote, Description, ReleaseDate }