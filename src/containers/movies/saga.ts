import { takeEvery, put } from "redux-saga/effects";
import { MoviesActionEnum } from "./types";
import { fetchData } from "../../utils/httpcalls";
import { apiUrl, apiKey } from "../../config";
import { Movie } from "../../models/movie";
import { fetchMoviesSuccess, fetchMoviesFail } from "./action";

function* fetchMovies() {
    try {
      const { results } = yield fetchData(`${apiUrl}discover/movie?api_key=${apiKey}`);
      const moviesList = [];
      for (let movie of results) {
        let newMovie = new Movie({ ...movie });
        moviesList.push(newMovie);
      }
      yield put(fetchMoviesSuccess(moviesList));
    } catch (error) {
      yield put(fetchMoviesFail(error));
    }
  }

  export function* watchMoviesSagas() {
    yield takeEvery(MoviesActionEnum.FETCH_MOVIES, fetchMovies);
  }