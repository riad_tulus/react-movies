import React from 'react';
import { CardList } from '../../component-ui/card-list';
import { Container } from './styles';
import { useMoviesFetch } from './hooks';
import { Movie } from '../../models/movie';
import { history } from '../../App';


const MoviesContainer = () => {

    // HANDLERS
    const handleFetchDetails = (movie: Movie) => history.push(`/${movie.id}`);

    // HOOKS
    const { movies, isLoading } = useMoviesFetch();

    // RENDERS
    return <Container><CardList isLoading={isLoading} movies={movies} fetchDetails={handleFetchDetails}/></Container>;
}

export { MoviesContainer };