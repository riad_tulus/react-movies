import { Movie } from "../../models/movie";
import { Action } from "redux";

export interface IMoviesState {
    movies: Array<Movie>;
    isLoading: boolean;
    errorMessage: string;
  }

export interface IFetchMovies extends Action {
    readonly type: MoviesActionEnum.FETCH_MOVIES;
  }
  
  export interface IFetchMoviesSuccess extends Action {
    readonly type: MoviesActionEnum.FETCH_MOVIES_SUCCESS;
    readonly payload: Array<Movie>;
  }
  
  export interface IFetchMoviesFail extends Action {
    readonly type: MoviesActionEnum.FETCH_MOVIES_FAIL;
    readonly payload: Error;
  }

  export enum MoviesActionEnum {
    FETCH_MOVIES = 'FETCH_MOVIES',
    FETCH_MOVIES_SUCCESS = 'FETCH_MOVIES_SUCCESS',
    FETCH_MOVIES_FAIL = 'FETCH_MOVIES_FAIL'
  }

  export type MoviesAction =
  | IFetchMovies
  | IFetchMoviesSuccess
  | IFetchMoviesFail
  ;