import { MoviesAction, MoviesActionEnum, IMoviesState } from "./types";
import { Movie } from "../../models/movie";

export const defaultMoviesState: IMoviesState = {
  movies: new Array<Movie>(),
  isLoading: false,
  errorMessage: "",
};

const movieDetailsReducer = (
  state: IMoviesState = defaultMoviesState,
  action: MoviesAction
): IMoviesState => {
  switch (action.type) {
    case MoviesActionEnum.FETCH_MOVIES:
      return {
        ...state,
        isLoading: true,
      };
    case MoviesActionEnum.FETCH_MOVIES_SUCCESS:
      return {
        ...state,
        isLoading: false,
        movies: action.payload,
      };
    case MoviesActionEnum.FETCH_MOVIES_FAIL:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload.message,
      };
    default:
      return state;
  }
};

export default movieDetailsReducer;
