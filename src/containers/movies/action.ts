import { IFetchMovies, MoviesActionEnum, IFetchMoviesSuccess, IFetchMoviesFail } from "./types";
import { Movie } from "../../models/movie";

export const fetchMovies = (): IFetchMovies => ({
    type: MoviesActionEnum.FETCH_MOVIES,
  });
  
  export const fetchMoviesSuccess = (payload: Array<Movie>): IFetchMoviesSuccess => ({
    type: MoviesActionEnum.FETCH_MOVIES_SUCCESS,
    payload,
  });
  
  export const fetchMoviesFail = (errorMessage: Error): IFetchMoviesFail => ({
    type: MoviesActionEnum.FETCH_MOVIES_FAIL,
    payload: errorMessage,
  });
  